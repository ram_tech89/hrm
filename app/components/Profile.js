import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import {
  Form,
  Input,
  Button,
  InputGroup,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  CardTitle,
  CardText,
  Row,
  Col
} from 'reactstrap'
import classnames from 'classnames'

export default class Profile extends Component {
  constructor(props) {
  super(props);

  this.toggle = this.toggle.bind(this);
  this.state = {
    activeTab: '1'
  };
}

toggle(tab) {
  if (this.state.activeTab !== tab) {
    this.setState({
      activeTab: tab
    });
  }
}
  render() {
    return (
        <div>

          <div className="am-pagetitle">
            <h5 className="am-title">Profile</h5>
          </div>

          <div className="am-mainpanel">
            <div className="am-pagebody">
              <div className="row">
                <div className="col-md-12">
                  <div className="card pd-20 bg-custom1">

                    <div className="row">
                      <div className="col-md-2" style={{'textAlign':'center'}}>
                        <img src="https://www.atomix.com.au/media/2015/06/atomix_user31.png" className="wd-100 rounded-circle" alt="" />
                        <br /><br />
                        <h6 className="tx-white tx-bold">User Name</h6>
                      </div>

                      <div className="col-md-4 tx-white tx-12">
                        <br />
                        <p className="tx-bold"><i className="fa fa-user"></i> Designation Here!</p>
                        <p className="tx-bold"><i className="icon ion-email"></i> Email Here!</p>
                        <p className="tx-bold"><i className="icon ion-android-phone-portrait"></i> Contact No.</p>
                      </div>
                      <div className="col-md-6 tx-white tx-12">
                        <br />
                        Other descriptions...
                      </div>
                    </div>

                  </div>
                </div>

                <div className="col-md-12">
                    <div className="pd-10 bg-custom2">
                      <Nav className="nav nav-gray-700" role="tablist">
                       <NavItem className="nav-item">
                         <NavLink
                           className={classnames({ active: this.state.activeTab === '1' })}
                           onClick={() => { this.toggle('1'); }}
                         >
                           <span className="nav-link tx-bold tx-14" data-toggle="tab"role="tab">General</span>
                         </NavLink>
                       </NavItem>

                       <NavItem>
                         <NavLink
                           className={classnames({ active: this.state.activeTab === '2' })}
                           onClick={() => { this.toggle('2'); }}
                         >
                           <span className="nav-link tx-bold tx-14" data-toggle="tab"role="tab">Contact</span>
                         </NavLink>
                       </NavItem>

                       <NavItem>
                         <NavLink
                           className={classnames({ active: this.state.activeTab === '3' })}
                           onClick={() => { this.toggle('3'); }}
                         >
                           <span className="nav-link tx-bold tx-14" data-toggle="tab"role="tab">Social</span>
                         </NavLink>
                       </NavItem>

                       <NavItem>
                         <NavLink
                           className={classnames({ active: this.state.activeTab === '4' })}
                           onClick={() => { this.toggle('4'); }}
                         >
                           <span className="nav-link tx-bold tx-14" data-toggle="tab"role="tab">Files</span>
                         </NavLink>
                       </NavItem>

                       <NavItem>
                         <NavLink
                           className={classnames({ active: this.state.activeTab === '5' })}
                           onClick={() => { this.toggle('5'); }}
                         >
                           <span className="nav-link tx-bold tx-14" data-toggle="tab"role="tab">Job Info</span>
                         </NavLink>
                       </NavItem>

                       <NavItem>
                         <NavLink
                           className={classnames({ active: this.state.activeTab === '6' })}
                           onClick={() => { this.toggle('6'); }}
                         >
                           <span className="nav-link tx-bold tx-14" data-toggle="tab"role="tab">Account Settings</span>
                         </NavLink>
                       </NavItem>

                     </Nav>
                     <TabContent activeTab={this.state.activeTab}>
                       <TabPane tabId="1">
                         <GeneralInfo />
                       </TabPane>

                       <TabPane tabId="2">
                         <ContactInfo />
                       </TabPane>

                       <TabPane tabId="3">
                         <SocialInfo />
                       </TabPane>

                       <TabPane tabId="4">
                         <FilesInfo />
                       </TabPane>

                       <TabPane tabId="5">
                         <JobInfo />
                       </TabPane>

                       <TabPane tabId="6">
                         <AccountSetting />
                       </TabPane>

                     </TabContent>
                    </div>
                </div>

              </div>
            </div>
          </div>

        </div>
    );
  }
}

class GeneralInfo extends React.Component {
  render() {
    return (
      <div className="pd-20">
        <div className="mg-t-20 mg-b-40">
          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            First Name : <Input type="text" className="tx-18 no-borders" placeholder="First Name" bsSize="sm" />
          </div>

          <div style={{'borderBottom':'1px dotted grey'}}>
            Last Name : <Input type="text" className="tx-18 no-borders" placeholder="Last Name" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Date Of Birth : <Input type="date" className="tx-18 no-borders" bsSize="sm" />
          </div>

          <div className="mg-t-20">
            <Button className="btn btn-success">Save</Button>
          </div>
        </div>
      </div>
    )
  }
}

class ContactInfo extends React.Component {
  render() {
    return (
      <div className="pd-20">
        <div className="mg-t-20 mg-b-40">

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Mailing Address : <Input type="text" className="tx-18 no-borders" bsSize="sm" placeholder="Address" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Alternative Address : <Input type="text" className="tx-18 no-borders" bsSize="sm" placeholder="Alternative Address" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Phone : <Input type="text" className="tx-18 no-borders" bsSize="sm" placeholder="Phone" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Alternative Phone : <Input type="text" className="tx-18 no-borders" bsSize="sm" placeholder="Alternative Phone" />
          </div>

          <div className="mg-t-20">
            <Button className="btn btn-success">Save</Button>
          </div>
        </div>
      </div>
    )
  }
}

class SocialInfo extends React.Component {
  render() {
    return (
      <div className="pd-20">
        <div className="mg-t-20 mg-b-40">

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Skype : <Input type="text" className="tx-18 no-borders" placeholder="email or username" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Facebook : <Input type="text" className="tx-18 no-borders" placeholder="email or username" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Twitter : <Input type="text" className="tx-18 no-borders" placeholder="email or username" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            GitHub : <Input type="text" className="tx-18 no-borders" placeholder="email or username" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Google : <Input type="text" className="tx-18 no-borders" placeholder="email or username" bsSize="sm" />
          </div>

          <div className="mg-t-20">
            <Button className="btn btn-success">Save</Button>
          </div>
        </div>
      </div>
    )
  }
}

class JobInfo extends React.Component {
  render() {
    return (
      <div className="pd-20">
        <div className="mg-t-20 mg-b-40">

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Job Title : <Input type="text" className="tx-18 no-borders" placeholder="Designation" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Salary : <Input type="text" className="tx-18 no-borders" placeholder="000000" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Salary Term : <Input type="text" className="tx-18 no-borders" placeholder="Monthly" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Date Of Hire : <Input type="date" className="tx-18 no-borders" placeholder="DD/MM/YYYY" bsSize="sm" />
          </div>

          <div className="mg-t-20">
            <Button className="btn btn-success">Save</Button>
          </div>
        </div>
      </div>
    )
  }
}

class FilesInfo extends React.Component {
  render() {
    return (
      <div className="pd-20">
        <div className="mg-t-20 mg-b-40">

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Title Here : <Input type="text" className="tx-18 no-borders" placeholder="Input" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Title Here :  <Input type="text" className="tx-18 no-borders" placeholder="Input" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Title Here :  <Input type="text" className="tx-18 no-borders" placeholder="Input" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Title Here :  <Input type="date" className="tx-18 no-borders" placeholder="Input" bsSize="sm" />
          </div>

          <div className="mg-t-20">
            <Button className="btn btn-success">Save</Button>
          </div>
        </div>
      </div>
    )
  }
}

class AccountSetting extends React.Component {
  render() {
    return (
      <div className="pd-20">
        <div className="mg-t-20 mg-b-40">

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Email : <Input type="email" className="tx-18 no-borders" placeholder="abc@abc.com" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Password :  <Input type="password" className="tx-18 no-borders" bsSize="sm" />
          </div>

          <div className="" style={{'borderBottom':'1px dotted grey'}}>
            Confirm Password :  <Input type="password" className="tx-18 no-borders" bsSize="sm" />
          </div>

          <div className="mg-t-20">
            <Button className="btn btn-success">Save</Button>
          </div>
        </div>
      </div>
    )
  }
}

