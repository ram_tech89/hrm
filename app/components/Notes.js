import React from 'react';
import { Row, Col } from 'react-bootstrap'
import { Sidebar, NavBar, SubContainer } from './'
import { Link } from 'react-router-dom'
import { Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, ButtonGroup, InputGroup, InputGroupAddon } from 'reactstrap'


export default class Notes extends React.Component {
  render() {
    return (
      <div>
        <div className="mainbody">
          <div className="am-pagetitle">
            <h5 className="am-title">Messages</h5>
          </div>
          {/*----------------main container of the page--------------*/}
            <div className="am-mainpanel">
              <div className="am-pagebody">
                <div className="card pd-20 pd-sm-20">

                  <div className="row">
                    <div className="col-md-10"><span className="tx-16 tx-inverse tx-bold">Notes(Private)</span></div>
                    <div className="col-md-2"><Button className="btn btn-sm btn-info pull-right"><i className="icon ion-plus-circled"></i> Add note</Button></div>
                      <div className="col-md-12">
                        < hr/>
                        <div className="row">
                          <div className="col-md-2">
                            <div className="form-group mg-b-10-force">
                            <Input className="form-control tx-12" type="select" bsSize="sm">
                              <option>10</option>
                              <option>20</option>
                              <option>50</option>
                              <option>100</option>
                            </Input>
                            </div>
                          </div>

                          <div className="col-md-7">
                          </div>

                          <div className="col-md-3">
                            <InputGroup size="sm">
                              <Input placeholder="Search notes..." />
                              <InputGroupAddon><i className="icon ion-search"></i></InputGroupAddon>
                            </InputGroup>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <Table inverse responsive>
                          <thead>
                            <tr>
                              <th>Date Created</th>
                              <th>Title</th>
                              <th className="wd-40"><i className="icon ion-navicon-round"></i></th>
                            </tr>
                          </thead>
                        </Table>
                      </div>
                      <div className="col-md-12 tx-12" style={{'textAlign':'center'}}>
                          No record found.
                      </div>
                      <div className="col-md-12">
                        < hr/>
                          0-0/0
                          <span className="pull-right">

                          <ButtonGroup>
                            <Button className="btn-info" size="sm"><i className="icon ion-ios-rewind-outline"></i></Button>{' '}
                            <Button className="btn-info" size="sm"><i className="icon ion-ios-fastforward-outline"></i></Button>
                          </ButtonGroup>

                          </span>
                      </div>
                  </div>
                </div>
              </div>
            </div>

        </div>
      </div>
    )
  }
}
