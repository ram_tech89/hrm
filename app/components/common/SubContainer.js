import React from 'react';
import { Row, Col } from 'react-bootstrap'
import { Sidebar, NavBar } from '../'
import { Link } from 'react-router-dom';
import {
  DashboardPage,
  ProfilePage,
  EmployeesPage,
  MessagesPage,
  NotesPage,
  TasksPage,
  LeavesPage,
  AnnouncementsPage,
  TimelinePage
 } from '../../containers';
import { Route } from 'react-router'


export default class SubContainer extends React.Component {
  render() {
    return (
      <div className="container-fluid">
        <Sidebar />
        <NavBar />
          <div>

            <Route exact path={`${this.props.match.url}/`} component={DashboardPage} />
            <Route path={`${this.props.match.url}/profile`} component={ProfilePage} />
            <Route path={`${this.props.match.url}/employees`} component={EmployeesPage} />
            <Route path={`${this.props.match.url}/messages`} component={MessagesPage} />
            <Route path={`${this.props.match.url}/notes`} component={NotesPage} />
            <Route path={`${this.props.match.url}/tasks`} component={TasksPage} />
            <Route path={`${this.props.match.url}/leaves`} component={LeavesPage} />
            <Route path={`${this.props.match.url}/announcements`} component={AnnouncementsPage} />
            <Route path={`${this.props.match.url}/timeline`} component={TimelinePage} />
          </div>
      </div>
    )
  }
}
