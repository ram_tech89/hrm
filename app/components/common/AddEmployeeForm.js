import React from 'react'
import { Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, FormText, InputGroup } from 'reactstrap'

export default class AddEmployeeForm extends React.Component {
  render() {
  return (
    <Form>
        <div className="row row-sm">

          <div className="col-md-6">

            <div className="card pd-20 form-layout form-layout-4">
                <h6 className="card-body-title">Add New Employee</h6>
                <p className="mg-b-20 mg-sm-b-30">General Information</p>

                <div className="row">
                  <label className="col-sm-4 form-control-label">Firstname <span className="tx-danger">*</span></label>
                  <div className="col-sm-8 mg-t-10 mg-sm-t-0">
                    <Input bsSize="sm" type="text" className="form-control" placeholder="Enter firstname"/>
                  </div>
                </div>

                <div className="row mg-t-20">
                  <label className="col-sm-4 form-control-label">Lastname <span className="tx-danger">*</span></label>
                  <div className="col-sm-8 mg-t-10 mg-sm-t-0">
                    <Input bsSize="sm" type="text" className="form-control" placeholder="Enter lastname"/>
                  </div>
                </div>

                <div className="row mg-t-20">
                  <label className="col-sm-4 form-control-label">Position: <span className="tx-danger">*</span></label>
                  <div className="col-sm-8 mg-t-10 mg-sm-t-0">
                    <Input bsSize="sm" type="text" className="form-control" placeholder="Position"/>
                  </div>
                </div>

                <div className="row mg-t-20">
                  <label className="col-sm-4 form-control-label">Start Date <span className="tx-danger">*</span></label>
                  <div className="col-sm-8 mg-t-10 mg-sm-t-0">
                    <Input bsSize="sm" type="date" className="form-control"/>
                  </div>
                </div><br />

              </div>
              &nbsp;

          </div>


          <div className="col-md-6">
            <div className="card pd-20 form-layout form-layout-4">
                <h6 className="card-body-title"> </h6>
                <p className="mg-b-20 mg-sm-b-30">Personal Information</p>

                <div className="row mg-t-20">
                  <label className="col-sm-4 form-control-label">Email <span className="tx-danger">*</span></label>
                  <div className="col-sm-8 mg-t-10 mg-sm-t-0">
                    <Input bsSize="sm" type="text" className="form-control" placeholder="Enter email address"/>
                  </div>
                </div>

                <div className="row mg-t-20">
                  <label className="col-sm-4 form-control-label">Contact<span className="tx-danger">*</span></label>
                  <div className="col-sm-8 mg-t-10 mg-sm-t-0">
                    <Input bsSize="sm" type="text" className="form-control" placeholder="Contact Number"/>
                  </div>
                </div>

                <div className="row mg-t-20">
                  <label className="col-sm-4 form-control-label">Address <span className="tx-danger">*</span></label>
                  <div className="col-sm-8 mg-t-10 mg-sm-t-0">
                    <textarea rows="1" className="form-control" placeholder="Enter your address"></textarea>
                  </div>
                </div>

                <div className="row mg-t-20">
                  <label className="col-sm-4 form-control-label">Picture <span className="tx-danger">*</span></label>
                  <div className="col-sm-8 mg-t-10 mg-sm-t-0">
                    <Input bsSize="sm" type="file" placeholder="Enter your address" />
                  </div>
                </div>

              </div>
          </div>

        </div>
    </Form>

  )
  }
}
