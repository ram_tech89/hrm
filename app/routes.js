/* eslint flowtype-errors/show-errors: 0 */
import React from 'react';
import { Switch, Route } from 'react-router';
import { App, HomePage, SignUpPage, ForgotPasswordPage , ResetPasswordPage, DashboardPage } from './containers';
import { SubContainer } from './components';

export default () => (
  <App>
    <Switch>
        <Route exact path="/" component={HomePage} />

        <Route path="/signup" component={SignUpPage} />
        <Route path="/forgotPassword" component={ForgotPasswordPage} />
        <Route path="/resetPassword" component={ResetPasswordPage} />

        <Route path="/dashboard" component={SubContainer} />
    </Switch>
  </App>
);
