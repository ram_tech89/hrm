// @flow
import React, { Component } from 'react';
import { ForgotPassword } from '../components';

export default class ForgotPasswordPage extends Component {
  render() {
    return (
      <ForgotPassword />
    );
  }
}
