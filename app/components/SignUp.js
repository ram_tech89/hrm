// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
const electron = require('electron');
const { remote, shell, BrowserWindow } = electron;
const { dialog } = remote;

export default class SignUp extends Component {
  onSubmit = () => {
    console.log(electron)

  }

  render() {
    return (
      <div className="am-signup-wrapper">
            <div className="am-signup-box">
              <div className="row no-gutters">
                <div className="col-lg-5">
                  <div>
                    <h2>HRM</h2>
                    <p>Welcome to CompanyName, Human Resource Management</p>
                    <p>Company text will be here. Company text will be here. Company text will be here. Company text will be here. </p>

                    <hr />
                    <p>Already have an account? <br /> <Link to="/">Sign In</Link></p>
                  </div>
                </div>
                <div className="col-lg-7">
                  <h5 className="tx-gray-800 mg-b-25">Signup</h5>

                  <div className="form-group">
                    <label className="form-control-label">Email</label>
                    <input type="email" name="email" className="form-control" placeholder="Type email address" />
                  </div>

                  <div className="row row-xs">
                    <div className="col">
                      <div className="form-group">
                        <label className="form-control-label">Firstname</label>
                        <input type="text" name="firstname" className="form-control" placeholder="Type firstname" />
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label className="form-control-label">Lastname</label>
                        <input type="text" name="lastname" className="form-control" placeholder="Type lastname" />
                      </div>
                    </div>
                  </div>

                  <div className="row row-xs">
                    <div className="col">
                      <div className="form-group">
                        <label className="form-control-label">Password</label>
                        <input type="password" name="password" className="form-control" placeholder="Type password" />
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-group">
                        <label className="form-control-label">Confirm Password</label>
                        <input type="password" name="conpassword" className="form-control" placeholder="Retype password" />
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="form-control-label">Company Name</label>
                    <input type="text" name="company" className="form-control" placeholder="Type company name" />
                  </div>

                  <div className="form-group mg-b-20 tx-12">By clicking Sign Up button below you agree to our <a href="">Terms of Use</a> and our <a href="">Privacy Policy</a></div>

                  <button type="submit" className="btn btn-block">Sign Up</button>
                </div>
              </div>
              <p className="tx-center tx-white-5 tx-12 mg-t-15">Copyright &copy; 2017. All Rights Reserved.</p>
            </div>
          </div>
    );
  }
}
