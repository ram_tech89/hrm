import React, { Component } from 'react';
import { Messages } from '../components';

export default class MessagesPage extends Component {
  render() {
    return (
      <Messages />
    );
  }
}
