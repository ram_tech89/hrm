// @flow
import React, { Component } from 'react';
import { Employees } from '../components';

export default class DashboardPage extends Component {
  render() {
    return (
      <Employees />
    );
  }
}
