import React from 'react';
import { Row, Col } from 'react-bootstrap'
import { Sidebar, NavBar, SubContainer } from './'
import { Link } from 'react-router-dom'


export default class Dashboard extends React.Component {
  render() {
    return (

      <div>
      <div className="am-mainpanel">
         <div className="am-pagetitle">
           <h5 className="am-title">Dashboard</h5>

         </div>

         <div className="am-pagebody">
           <div className="row row-sm">
             <div className="col-lg-4">
               <div className="card bg-info tx-white">
                 <div id="rs1" className="wd-100p ht-200"></div>
                 <div className="overlay-body pd-x-20 pd-t-20">
                   <div className="d-flex justify-content-between">
                     <div>
                       <h6 className="tx-12 tx-uppercase tx-bold mg-b-5">Todays Earnings</h6>
                       <p className="tx-12">November 21, 2017</p>
                     </div>
                     <a href="" className="tx-gray-600 hover-info"><i className="fa fa-ellipsis-h tx-16 lh-0"></i></a>
                   </div>
                   <h2 className="mg-b-5 tx-lato">$12,212</h2>
                   <p className="tx-12 mg-b-0">Earnings before taxes.</p>
                 </div>
               </div>
             </div>
             <div className="col-lg-4 mg-t-15 mg-sm-t-20 mg-lg-t-0">
               <div className="card bg-success tx-white">
                 <div id="rs2" className="wd-100p ht-200"></div>
                 <div className="overlay-body pd-x-20 pd-t-20">
                   <div className="d-flex justify-content-between">
                     <div>
                       <h6 className="tx-12 tx-uppercase tx-bold mg-b-5">This Weeks Earnings</h6>
                       <p className="tx-12">November 20 - 27, 2017</p>
                     </div>
                     <a href="" className="tx-grey-600 hover-info"><i className="fa fa-ellipsis-h tx-16 lh-0"></i></a>
                   </div>
                   <h2 className="mg-b-5 tx-lato">$28,746</h2>
                   <p className="tx-12 mg-b-0">Earnings before taxes.</p>
                 </div>
               </div>
             </div>
             <div className="col-lg-4 mg-t-15 mg-sm-t-20 mg-lg-t-0">
               <div className="card bg-dark tx-white">
                 <div id="rs3" className="wd-100p ht-200"></div>
                 <div className="overlay-body pd-x-20 pd-t-20">
                   <div className="d-flex justify-content-between">
                     <div>
                       <h6 className="tx-12 tx-uppercase tx-bold mg-b-5">This Months Earnings</h6>
                       <p className="tx-12">November 1 - 30, 2017</p>
                     </div>
                     <a href="" className="tx-gray-600 hover-info"><i className="fa fa-ellipsis-h tx-16 lh-0"></i></a>
                   </div>
                   <h2 className="mg-b-5 tx-lato">$72,118</h2>
                   <p className="tx-12 mg-b-0">Earnings before taxes.</p>
                 </div>
               </div>
             </div>
           </div>

           <div className="row row-sm mg-t-15 mg-sm-t-20">
             <div className="col-lg-8">
               <div className="card">
                 <div className="card-header bg-transparent pd-20">
                   <h6 className="card-title tx-uppercase tx-12 mg-b-0">User Transaction History</h6>
                 </div>
                 <div className="table-responsive">
                   <table className="table table-white mg-b-0 tx-12">
                     <tbody>
                       <tr>
                         <td className="pd-l-20 tx-center">
                           <img src="https://cdn0.iconfinder.com/data/icons/PRACTIKA/256/user.png" className="wd-36 rounded-circle" alt="Image" />
                         </td>
                         <td>
                           <a href="" className="tx-inverse tx-14 tx-medium d-block">Mark K. Peters</a>
                           <span className="tx-11 d-block">TRANSID: 1234567890</span>
                         </td>
                         <td className="tx-12">
                           <span className="square-8 bg-success mg-r-5 rounded-circle"></span> Email verified
                         </td>
                         <td>Just Now</td>
                       </tr>
                       <tr>
                         <td className="pd-l-20 tx-center">
                           <img src="https://cdn0.iconfinder.com/data/icons/PRACTIKA/256/user.png" className="wd-36 rounded-circle" alt="Image" />
                         </td>
                         <td>
                           <a href="" className="tx-inverse tx-14 tx-medium d-block">Karmen F. Brown</a>
                           <span className="tx-11 d-block">TRANSID: 1234567890</span>
                         </td>
                         <td className="tx-12">
                           <span className="square-8 bg-warning mg-r-5 rounded-circle"></span> Pending verification
                         </td>
                         <td>Apr 21, 2017 8:34am</td>
                       </tr>
                       <tr>
                         <td className="pd-l-20 tx-center">
                           <img src="https://cdn0.iconfinder.com/data/icons/PRACTIKA/256/user.png" className="wd-36 rounded-circle" alt="Image" />
                         </td>
                         <td>
                           <a href="" className="tx-inverse tx-14 tx-medium d-block">Gorgonio Magalpok</a>
                           <span className="tx-11 d-block">TRANSID: 1234567890</span>
                         </td>
                         <td className="tx-12">
                           <span className="square-8 bg-success mg-r-5 rounded-circle"></span> Purchased success
                         </td>
                         <td>Apr 10, 2017 4:40pm</td>
                       </tr>
                       <tr>
                         <td className="pd-l-20 tx-center">
                           <img src="https://cdn0.iconfinder.com/data/icons/PRACTIKA/256/user.png" className="wd-36 rounded-circle" alt="Image" />
                         </td>
                         <td>
                           <a href="" className="tx-inverse tx-14 tx-medium d-block">Ariel T. Hall</a>
                           <span className="tx-11 d-block">TRANSID: 1234567890</span>
                         </td>
                         <td className="tx-12">
                           <span className="square-8 bg-warning mg-r-5 rounded-circle"></span> Payment on hold
                         </td>
                         <td>Apr 02, 2017 6:45pm</td>
                       </tr>
                       <tr>
                         <td className="pd-l-20 tx-center">
                           <img src="https://cdn0.iconfinder.com/data/icons/PRACTIKA/256/user.png" className="wd-36 rounded-circle" alt="Image" />
                         </td>
                         <td>
                           <a href="" className="tx-inverse tx-14 tx-medium d-block">John L. Goulette</a>
                           <span className="tx-11 d-block">TRANSID: 1234567890</span>
                         </td>
                         <td className="tx-12">
                           <span className="square-8 bg-pink mg-r-5 rounded-circle"></span> Account deactivated
                         </td>
                         <td>Mar 30, 2017 10:30am</td>
                       </tr>
                       <tr>
                         <td className="pd-l-20 tx-center">
                           <img src="https://cdn0.iconfinder.com/data/icons/PRACTIKA/256/user.png" className="wd-36 rounded-circle" alt="Image" />
                         </td>
                         <td>
                           <a href="" className="tx-inverse tx-14 tx-medium d-block">John L. Goulette</a>
                           <span className="tx-11 d-block">TRANSID: 1234567890</span>
                         </td>
                         <td className="tx-12">
                           <span className="square-8 bg-pink mg-r-5 rounded-circle"></span> Account deactivated
                         </td>
                         <td>Mar 30, 2017 10:30am</td>
                       </tr>
                     </tbody>
                   </table>
                 </div>
                 <div className="card-footer tx-12 pd-y-15 bg-transparent bd-t bd-gray-200">
                   <Link to="/dashboard"><i className="fa fa-angle-down mg-r-5"></i>View All Transaction History</Link>
                 </div>
               </div>
             </div>
             <div className="col-lg-4 mg-t-15 mg-sm-t-20 mg-lg-t-0">
               <div className="card pd-20">
                 <h6 className="tx-12 tx-uppercase tx-inverse tx-bold mg-b-15">Sales Report</h6>
                 <div className="d-flex mg-b-10">
                   <div className="bd-r pd-r-10">
                     <label className="tx-12">Today</label>
                     <p className="tx-lato tx-inverse tx-bold">1,898</p>
                   </div>
                   <div className="bd-r pd-x-10">
                     <label className="tx-12">This Week</label>
                     <p className="tx-lato tx-inverse tx-bold">32,112</p>
                   </div>
                   <div className="pd-l-10">
                     <label className="tx-12">This Month</label>
                     <p className="tx-lato tx-inverse tx-bold">72,067</p>
                   </div>
                 </div>
                 <div className="progress mg-b-10">
                   <div className="progress-bar wd-50p" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
                 </div>
                 <p className="tx-12 mg-b-0">Maecenas tempus, tellus eget condimentum rhoncus</p>
               </div>

               <ul className="list-group widget-list-group bg-info mg-t-20">
                 <li className="list-group-item rounded-top-0">
                   <p className="mg-b-0"><i className="fa fa-cube tx-white-7 mg-r-8"></i><strong className="tx-medium">Retina: 13.3-inch</strong> <span className="text-muted">Display</span></p>
                 </li>
                 <li className="list-group-item">
                   <p className="mg-b-0"><i className="fa fa-cube tx-white-7 mg-r-8"></i><strong className="tx-medium">Intel Iris Graphics 6100</strong> <span className="text-muted">Graphics</span></p>
                 </li>
                 <li className="list-group-item">
                   <p className="mg-b-0"><i className="fa fa-cube tx-white-7 mg-r-8"></i><strong className="tx-medium">500 GB</strong> <span className="text-muted">Flash Storage</span></p>
                 </li>
                 <li className="list-group-item">
                   <p className="mg-b-0"><i className="fa fa-cube tx-white-7 mg-r-8"></i><strong className="tx-medium">3.1 GHz Intel Core i7</strong> <span className="text-muted">Processor</span></p>
                 </li>
                 <li className="list-group-item rounded-bottom-0">
                   <p className="mg-b-0"><i className="fa fa-cube tx-white-7 mg-r-8"></i><strong className="tx-tx-medium">16 GB 1867 MHz DDR3</strong> <span className="text-muted">Memory</span></p>
                 </li>
                 <li className="list-group-item rounded-bottom-0">
                   <p className="mg-b-0"><i className="fa fa-cube tx-white-7 mg-r-8"></i><strong className="tx-tx-medium">Mi Note 4G</strong> <span className="text-muted">Memory Max</span></p>
                 </li>
               </ul>
             </div>
           </div>

         </div>
         <div className="am-footer">
           <span>Copyright &copy;. All Rights Reserved.</span>
           <span>HRM</span>
         </div>
       </div>
      </div>
    )
  }
}
