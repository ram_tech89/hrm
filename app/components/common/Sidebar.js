import React from 'react'
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Input } from 'reactstrap';
import { Link } from 'react-router-dom'
import classnames from 'classnames';

export default class Sidebar extends React.Component{
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    return (
      <div className="am-sideleft tx-12">




      <div>
             <Nav>
              <ul className="nav am-sideleft-tab">

               <NavItem>
                 <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.toggle('1'); }}>
                     <span><i className="icon ion-ios-home-outline tx-24"></i></span>
                 </NavLink>
               </NavItem>

               <NavItem>
                 <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.toggle('2'); }} >
                     <span><i className="icon ion-ios-email-outline tx-24"></i></span>
                 </NavLink>
               </NavItem>

               <NavItem>
                 <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.toggle('3'); }} >
                     <span><i className="icon ion-ios-chatboxes-outline tx-24"></i></span>
                 </NavLink>
               </NavItem>

               <NavItem>
                 <NavLink className={classnames({ active: this.state.activeTab === '4' })} onClick={() => { this.toggle('4'); }} >
                     <span><i className="icon ion-ios-gear-outline tx-24"></i></span>
                 </NavLink>
               </NavItem>

              </ul>
             </Nav>
             <TabContent activeTab={this.state.activeTab}>

               <TabPane tabId="1">
                 {/*--The component tab--*/}
                 <DashboardTab />
               </TabPane>

               <TabPane tabId="2">
                 {/*--The component tab--*/}
                 <EmailTab />
               </TabPane>

               <TabPane tabId="3">
                 {/*--The component tab--*/}
                 <ChatTab />
               </TabPane>

               <TabPane tabId="4">
                 {/*--The component tab--*/}
                 <SettingTab />
               </TabPane>

             </TabContent>
           </div>
      </div>
    )
  }
}


class DashboardTab extends React.Component {
  render() {
    return (
      <div>
        <div id="mainMenu">
          <ul className="nav am-sideleft-menu">

            <li className="nav-item">
              <Link to="/dashboard" className="nav-link">
                <i className="icon ion-ios-speedometer-outline mg-l-3"></i>
                <span> Dashboard</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/dashboard/timeline" className="nav-link">
                <i className="icon ion-ios-calendar-outline mg-l-3"></i>
                <span> Timeline</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/dashboard/employees" className="nav-link">
                <i className="icon ion-ios-people-outline mg-l-3"></i>
                <span> Employees</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/dashboard/messages" className="nav-link">
                <i className="icon ion-ios-paperplane-outline mg-l-3"></i>
                <span> Messages</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/dashboard/tasks" className="nav-link">
                <i className="icon ion-ios-list-outline mg-l-3"></i>
                <span> Tasks</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/dashboard/notes" className="nav-link">
                <i className="icon ion-ios-paper-outline mg-l-3"></i>
                <span> Notes</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/dashboard/leaves" className="nav-link">
                <i className="icon ion-log-out mg-l-3"></i>
                <span> Leaves</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/dashboard" className="nav-link">
                <i className="icon ion-ios-copy-outline mg-l-3"></i>
                <span> Reports</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/dashboard/announcements" className="nav-link">
                <i className="icon ion-speakerphone mg-l-3"></i>
                <span> Announcements</span>
              </Link>
            </li>

            <li className="nav-item">
              <Link to="/dashboard" className="nav-link">
                <i className="icon ion-ios-help-outline mg-l-3"></i>
                <span> Help & Support</span>
              </Link>
            </li>

          </ul>
        </div>
    </div>
    )
  }
}

class EmailTab extends React.Component {
  render() {
    return (
      <div id="emailMenu">

        <ul className="nav am-sideleft-menu">
          <li className="nav-item">
            <a href="" className="nav-link">
              <i className="icon ion-ios-filing-outline"></i>
              <span>Inbox</span>
            </a>
          </li>
          <li className="nav-item">
            <a href="" className="nav-link">
              <i className="icon ion-ios-filing-outline"></i>
              <span>Drafts</span>
            </a>
          </li>
          <li className="nav-item">
            <a href="" className="nav-link">
              <i className="icon ion-ios-paperplane-outline"></i>
              <span>Sent</span>
            </a>
          </li>
          <li className="nav-item">
            <a href="" className="nav-link">
              <i className="icon ion-ios-trash-outline"></i>
              <span>Trash</span>
            </a>
          </li>
          <li className="nav-item">
            <a href="" className="nav-link">
              <i className="icon ion-ios-filing-outline"></i>
              <span>Spam</span>
            </a>
          </li>
        </ul>

        <label className="pd-x-20 tx-uppercase tx-11 mg-t-10 tx-orange mg-b-0 tx-medium">My Folder</label>
        <ul className="nav am-sideleft-menu">
          <li className="nav-item">
            <a href="" className="nav-link">
              <i className="icon ion-ios-folder-outline"></i>
              <span>Updates</span>
            </a>
          </li>
          <li className="nav-item">
            <a href="" className="nav-link">
              <i className="icon ion-ios-folder-outline"></i>
              <span>Social</span>
            </a>
          </li>
          <li className="nav-item">
            <a href="" className="nav-link">
              <i className="icon ion-ios-folder-outline"></i>
              <span>Promotions</span>
            </a>
          </li>
        </ul>
      </div>
    )
  }
}

class ChatTab extends React.Component {
  render() {
    return (
      <div id="chatMenu">
        <div className="chat-search-bar">
          <Input type="search" className="form-control wd-150" placeholder="Search chat..." />
          <button className="btn btn-secondary"><i className="fa fa-search"></i></button>
        </div>

        <label className="pd-x-15 tx-uppercase tx-11 mg-t-20 tx-orange mg-b-10 tx-medium">Recent Chat History</label>
        <div className="list-group list-group-chat">
          <span className="list-group-item">
            <div className="d-flex align-items-center">
              <img src="https://i.pinimg.com/736x/a2/e1/8c/a2e18cbfbcaa8756fe5b40f472eeff45--profile-picture-profile-pics.jpg" className="wd-32 rounded-circle" alt="" />
              <div className="mg-l-10">
                <h6>June M. Evans</h6>
                <span>Tuesday, 10:33am</span>
              </div>
            </div>
            <p>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain...</p>
          </span>
          <span className="list-group-item">
            <div className="d-flex align-items-center">
              <img src="https://data.whicdn.com/images/277973909/original.jpg" className="wd-32 rounded-circle" alt="" />
              <div className="mg-l-10">
                <h6>Linda D. Sears</h6>
                <span>Monday, 4:21pm</span>
              </div>
            </div>
            <p>But who has any right to find fault with a man who chooses to enjoy a pleasure that has...</p>
          </span>
          <span className="list-group-item">
            <div className="d-flex align-items-center">
              <img src="https://pbs.twimg.com/profile_images/378800000079369168/3de6bcdff925153b31bea885111289f9_400x400.png" className="wd-32 rounded-circle" alt="" />
              <div className="mg-l-10">
                <h6>Sharon R. Rowe</h6>
                <span>Sunday, 7:45pm</span>
              </div>
            </div>
            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising...</p>
          </span>
          <span className="list-group-item">
            <div className="d-flex align-items-center">
              <img src="http://1.bp.blogspot.com/-sjaLwksjxe4/UJLAoua1_nI/AAAAAAAABaM/82WAowBg1oE/s640/facebook-profile-pictures23-420x504.jpg" className="wd-32 rounded-circle" alt="" />
              <div className="mg-l-10">
                <h6>Ruby M. Martin</h6>
                <span>Sunday, 7:45pm</span>
              </div>
            </div>
            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising...</p>
          </span>
          <span className="list-group-item">
            <div className="d-flex align-items-center">
              <img src="http://www.mulierchile.com/profile-pictures/profile-pictures-009.jpg" className="wd-32 rounded-circle" alt="" />
              <div className="mg-l-10">
                <h6>Andy C. Mayo</h6>
                <span>Sunday, 7:45pm</span>
              </div>
            </div>
            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising...</p>
          </span>
        </div>
        <span className="d-block pd-15 tx-12">Loading messages...</span>

      </div>
    )
  }
}

class SettingTab extends React.Component {
  render() {
    return (
      <div id="settingMenu">
        <div className="pd-x-15">
          <label className="tx-uppercase tx-11 mg-t-10 tx-orange mg-b-15 tx-medium">Quick Settings</label>
          <div className="bd pd-15">
            <h6 className="tx-13 tx-normal tx-gray-800">Daily Newsletter</h6>
            <p className="tx-12">Get notified when someone else is trying to access your account.</p>
            <div className="toggle toggle-light warning">On Off</div>
          </div>

          <div className="bd pd-15 mg-t-15">
            <h6 className="tx-13 tx-normal tx-gray-800">Call Phones</h6>
            <p className="tx-12">Make calls to friends and family right from your account.</p>
            <div className="toggle toggle-light warning">On Off</div>
          </div>

          <div className="bd pd-15 mg-t-15">
            <h6 className="tx-13 tx-normal tx-gray-800">Login Notifications</h6>
            <p className="tx-12">Get notified when someone else is trying to access your account.</p>
            <div className="toggle toggle-light warning">On Off</div>
          </div>

          <div className="bd pd-15 mg-t-15">
            <h6 className="tx-13 tx-normal tx-gray-800">Phone Approvals</h6>
            <p className="tx-12">Use your phone when login as an extra layer of security.</p>
            <div className="toggle toggle-light warning">On Off</div>
          </div>
        </div>
      </div>
    )
  }
}
