import React from 'react';
import { Sidebar, NavBar, SubContainer } from './'
import { Link } from 'react-router-dom'
import {
  Button,
  Input,
  ButtonGroup,
  InputGroup,
  InputGroupAddon,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  CardTitle,
  CardText,
  Row,
  Col,
  Table
 } from 'reactstrap'
 import classnames from 'classnames'


export default class Announcements extends React.Component {
  constructor(props) {
  super(props);

  this.toggle = this.toggle.bind(this);
  this.state = {
    activeTab: '1'
  };
}

toggle(tab) {
  if (this.state.activeTab !== tab) {
    this.setState({
      activeTab: tab
    });
  }
}
  render() {
    return (
      <div>
        <div className="mainbody">
          <div className="am-pagetitle">
            <h5 className="am-title">Announcements</h5>
          </div>
          {/*----------------main container of the page--------------*/}
            <div className="am-mainpanel">
              <div className="am-pagebody">
                <h6></h6>
                <div className="card pd-sm-20">
                  <div className="row">

                      <div className="col-md-12">
                      <br />
                        <div className="row">
                          <div className="col-md-2">
                            <div className="form-group mg-b-10-force">
                              <Input className="form-control wd-60 tx-12" type="select" bsSize="sm">
                                <option>10</option>
                                <option>20</option>
                                <option>50</option>
                                <option>100</option>
                              </Input>
                            </div>
                          </div>

                          <div className="col-md-6">

                          </div>



                          <div className="col-md-1">
                            <div className="form-group mg-b-10-force">
                              <Button className="btn btn-sm">Print</Button>
                            </div>
                          </div>

                          <div className="col-md-3">
                            <InputGroup  size="sm" style={{'borderRadius':'0px'}}>
                              <Input placeholder="Search..." />
                              <InputGroupAddon><i className="icon ion-search"></i></InputGroupAddon>
                            </InputGroup>
                          </div>

                        </div>
                      </div>


                      <div className="col-md-12">
                        <Table inverse responsive>
                          <thead>
                            <tr>
                              <th>Title</th>
                              <th>Created By</th>
                              <th>Start Date</th>
                              <th>End Date</th>
                              <th><i className="icon ion-navicon-round pull-right"></i></th>
                            </tr>
                          </thead>
                        </Table>
                      </div>


                      <div className="col-md-12 tx-12" style={{'textAlign':'center'}}>
                          No record found.
                      </div>
                      <div className="col-md-12">
                        < hr/>
                          0-0/0
                          <span className="pull-right">

                          <ButtonGroup>
                            <Button className="btn-info" size="sm"><i className="icon ion-ios-rewind-outline"></i></Button>{' '}
                            <Button className="btn-info" size="sm"><i className="icon ion-ios-fastforward-outline"></i></Button>
                          </ButtonGroup>

                          </span>
                      </div>
                  </div>
                </div>
              </div>
            </div>

        </div>
      </div>
    )
  }
}
