export Home from './Home';

export SignUp from './SignUp';
export ForgotPassword from './ForgotPassword';
export ResetPassword from './ResetPassword';

export Profile from './Profile'

export Dashboard from './Dashboard';
export Sidebar from './common/Sidebar';
export NavBar from './common/NavBar';
export SubContainer from './common/SubContainer'
export AddEmployeeForm from './common/AddEmployeeForm'

export Employees from './Employees'
export Messages from './Messages'
export Notes from './Notes'
export Tasks from './Tasks'
export Leaves from './Leaves'
export Announcements from './Announcements'
export Timeline from './Timeline'
