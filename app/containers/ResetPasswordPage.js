// @flow
import React, { Component } from 'react';
import { ResetPassword } from '../components';

export default class ResetPasswordPage extends Component {
  render() {
    return (
      <ResetPassword />
    );
  }
}
