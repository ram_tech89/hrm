import React from 'react';
import { Sidebar, NavBar, SubContainer } from './'
import { Link } from 'react-router-dom'
import {
  Button,
  Input,
  ButtonGroup,
  InputGroup,
  InputGroupAddon,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  CardTitle,
  CardText,
  Row,
  Col,
  Table
 } from 'reactstrap'
 import classnames from 'classnames'


export default class Leaves extends React.Component {
  constructor(props) {
  super(props);

  this.toggle = this.toggle.bind(this);
  this.state = {
    activeTab: '1'
  };
}

toggle(tab) {
  if (this.state.activeTab !== tab) {
    this.setState({
      activeTab: tab
    });
  }
}
  render() {
    return (
      <div>
        <div className="mainbody">
          <div className="am-pagetitle">
            <h5 className="am-title">My Leaves</h5>
          </div>
          {/*----------------main container of the page--------------*/}
            <div className="am-mainpanel">
              <div className="am-pagebody">
                <div className="card pd-sm-20">
                <div className="col-md-12">
                  <Button className="btn btn-sm pull-right"><i className="icon ion-plus-circled"></i> Apply Leave</Button>
                </div>
                      <Nav tabs>
                        <NavItem>
                          <NavLink
                            className={classnames({ active: this.state.activeTab === '1' })}
                            onClick={() => { this.toggle('1'); }}
                          >
                            <span className="tx-bold">Monthly</span>
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames({ active: this.state.activeTab === '2' })}
                            onClick={() => { this.toggle('2'); }}
                          >
                            <span  className="tx-bold">Yearly</span>
                          </NavLink>
                        </NavItem>
                      </Nav>
                      <TabContent activeTab={this.state.activeTab}>

                        <TabPane tabId="1">
                          <Monthly />
                        </TabPane>

                        <TabPane tabId="2">
                          <Yearly />
                        </TabPane>
                      </TabContent>

                </div>
              </div>
            </div>

        </div>
      </div>
    )
  }
}

class Monthly extends React.Component {
  render() {
    return (
      <div>

        <div className="row">

            <div className="col-md-12">
            <br />
              <div className="row">
                <div className="col-md-2">
                  <div className="form-group mg-b-10-force">
                    <Input className="form-control wd-60 tx-12" type="select" bsSize="sm">
                      <option>10</option>
                      <option>20</option>
                      <option>50</option>
                      <option>100</option>
                    </Input>
                  </div>
                </div>

                <div className="col-md-3">

                </div>

                <div className="col-md-3">
                    <ButtonGroup size="sm" className="pull-right">
                      <Button><i className="icon ion-chevron-left"></i></Button>
                      <Button>November 2017</Button>
                      <Button><i className="icon ion-chevron-right"></i></Button>
                    </ButtonGroup>
                </div>

                <div className="col-md-1">
                  <div className="form-group mg-b-10-force">
                    <Button className="btn btn-sm">Print</Button>
                  </div>
                </div>

                <div className="col-md-3">
                  <InputGroup  size="sm" style={{'borderRadius':'0px'}}>
                    <Input placeholder="Search..." />
                    <InputGroupAddon><i className="icon ion-search"></i></InputGroupAddon>
                  </InputGroup>
                </div>

              </div>
            </div>


            <div className="col-md-12">
              <Table inverse responsive>
                <thead>
                  <tr>
                    <th>Leave type</th>
                    <th>date</th>
                    <th>Duration</th>
                    <th>Status</th>
                    <th><i className="icon ion-navicon-round pull-right"></i></th>
                  </tr>
                </thead>
              </Table>
            </div>


            <div className="col-md-12 tx-12" style={{'textAlign':'center'}}>
                No record found.
            </div>
            <div className="col-md-12">
              < hr/>
                0-0/0
                <span className="pull-right">

                <ButtonGroup>
                  <Button className="btn-info" size="sm"><i className="icon ion-ios-rewind-outline"></i></Button>{' '}
                  <Button className="btn-info" size="sm"><i className="icon ion-ios-fastforward-outline"></i></Button>
                </ButtonGroup>

                </span>
            </div>
        </div>
      </div>
    )
  }
}

class Yearly extends React.Component {
  render() {
    return (
      <div>

        <div className="row">

            <div className="col-md-12">
            <br />
              <div className="row">
                <div className="col-md-2">
                  <div className="form-group mg-b-10-force">
                    <Input className="form-control wd-60 tx-12" type="select" bsSize="sm">
                      <option>10</option>
                      <option>20</option>
                      <option>50</option>
                      <option>100</option>
                    </Input>
                  </div>
                </div>

                <div className="col-md-3">

                </div>

                <div className="col-md-3">
                    <ButtonGroup size="sm" className="pull-right">
                      <Button><i className="icon ion-chevron-left"></i></Button>
                      <Button>2017</Button>
                      <Button><i className="icon ion-chevron-right"></i></Button>
                    </ButtonGroup>
                </div>

                <div className="col-md-1">
                  <div className="form-group mg-b-10-force">
                    <Button className="btn btn-sm">Print</Button>
                  </div>
                </div>

                <div className="col-md-3">
                  <InputGroup  size="sm" style={{'borderRadius':'0px'}}>
                    <Input placeholder="Search..." />
                    <InputGroupAddon><i className="icon ion-search"></i></InputGroupAddon>
                  </InputGroup>
                </div>

              </div>
            </div>


            <div className="col-md-12">
              <Table inverse responsive>
                <thead>
                  <tr>
                    <th>Leave type</th>
                    <th>date</th>
                    <th>Duration</th>
                    <th>Status</th>
                    <th><i className="icon ion-navicon-round pull-right"></i></th>
                  </tr>
                </thead>
              </Table>
            </div>


            <div className="col-md-12 tx-12" style={{'textAlign':'center'}}>
                No record found.
            </div>
            <div className="col-md-12">
              < hr/>
                0-0/0
                <span className="pull-right">

                <ButtonGroup>
                  <Button className="btn-info" size="sm"><i className="icon ion-ios-rewind-outline"></i></Button>{' '}
                  <Button className="btn-info" size="sm"><i className="icon ion-ios-fastforward-outline"></i></Button>
                </ButtonGroup>

                </span>
            </div>
        </div>
      </div>
    )
  }
}
