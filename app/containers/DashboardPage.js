// @flow
import React, { Component } from 'react';
import { Dashboard } from '../components';

export default class DashboardPage extends Component {
  render() {
    return (
      <Dashboard />
    );
  }
}
