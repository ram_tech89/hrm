// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Form, Button, FormControl, FormGroup } from 'react-bootstrap';
const electron = require('electron');
const { remote, shell, BrowserWindow } = electron;
const { dialog } = remote;

export default class Home extends Component {
  onSubmit = () => {
    console.log(electron)

  }

  render() {
    return (
      <div className="am-signin-wrapper">
        <div className="am-signin-box">
          <div className="row no-gutters">
            <div className="col-lg-5">
              <div>
                <h2>HRM</h2>
                <p>Welcome to CompanyName, Human Resource Management</p>
                <p>Company text will be here. Company text will be here. Company text will be here. Company text will be here. </p>

                <hr />
                <p>Dont have an account? <br /> <Link to="/signup">Sign Up</Link></p>
              </div>
            </div>
            <div className="col-lg-7">
              <h5 className="tx-gray-800 mg-b-25">Signin to Your Account</h5>

              <div className="form-group">
                <label className="form-control-label">Email</label>
                <input type="email" name="email" className="form-control" placeholder="Enter your email address" />
              </div>

              <div className="form-group">
                <label className="form-control-label">Password</label>
                <input type="password" name="password" className="form-control" placeholder="Enter your password" />
              </div>

              <div className="form-group mg-b-20"><Link to="/forgotPassword">Forgot Password?</Link></div>

              <button type="submit" className="btn btn-block">Sign In</button>
              <br />
              temp <Link to="/dashboard">Dashboard</Link>
            </div>
          </div>
          <p className="tx-center tx-white-5 tx-12 mg-t-15">Copyright &copy; 2017. All Rights Reserved. HRM</p>
        </div>
    </div>
    );
  }
}
