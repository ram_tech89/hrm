import React, { Component } from 'react';
import { Announcements } from '../components';

export default class AnnouncementsPage extends Component {
  render() {
    return (
      <Announcements />
    );
  }
}
