// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Form, Button, FormControl, FormGroup } from 'react-bootstrap';
const electron = require('electron');
const { remote, shell, BrowserWindow } = electron;
const { dialog } = remote;

export default class ResetPassword extends Component {
  onSubmit = () => {
    console.log(electron)

  }

  render() {
    return (
      <div className="am-signin-wrapper">
        <div className="am-signin-box">
          <div className="row no-gutters">
            <div className="col-lg-5">
              <div>
                <h2>HRM</h2>
                <p>Reset Your Password</p>

              </div>
            </div>
            <div className="col-lg-7">
              <h5 className="tx-gray-800 mg-b-25">Reset Password</h5>

              <div className="form-group">
                <label className="form-control-label">New Password</label>
                <input type="email" name="email" className="form-control" placeholder="Enter New Password" />
              </div>

              <div className="form-group">
                <label className="form-control-label">Confirm New Password</label>
                <input type="password" name="password" className="form-control" placeholder="Enter New Password again" />
              </div>

              <button type="submit" className="btn btn-block">Reset</button>
              temp <Link to="/">Login</Link>
            </div>
          </div>
          <p className="tx-center tx-white-5 tx-12 mg-t-15">Copyright &copy; 2017. All Rights Reserved. HRM</p>
        </div>
    </div>
    );
  }
}
