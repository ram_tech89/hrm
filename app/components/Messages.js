import React from 'react';
import { Row, Col } from 'react-bootstrap'
import { Sidebar, NavBar, SubContainer } from './'
import { Link } from 'react-router-dom'
import { Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, InputGroup, InputGroupAddon } from 'reactstrap'


export default class Messages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
}
  render() {
    return (
      <div>
        <div className="mainbody">
          <div className="am-pagetitle">
            <h5 className="am-title">Notes</h5>
            </div>

            <div className="am-mainpanel">
              <div className="am-pagebody">

                  <div className="row row-sm">
                    <div className="col-lg-2">
                      <div className="">
                        <div className="wd-100p ht-200"></div>
                        <div className="overlay-body pd-x-20 pd-t-20">
                          <div className="d-flex justify-content-between">
                            <div>
                              <div>
                                <Button className="btn btn-link" onClick={this.toggle}>Compose</Button>
                                <Modal style={{'width' : '500px','maxWidth':'500px'}} isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                                  <ModalHeader toggle={this.toggle}>Send Message</ModalHeader>
                                    <ModalBody>
                                      <ComposeMailForm />
                                    </ModalBody>
                                    <ModalFooter>
                                      <Button className="tx-12" color="info" onClick={this.toggle}><i className="icon ion-paper-airplane"></i> Send</Button>{' '}
                                      <Button className="tx-12" onClick={this.toggle}><i className="icon ion-close"></i> Cancel</Button>
                                    </ModalFooter>
                                </Modal>
                              </div>

                              <div>
                                <button className="btn btn-link">Inbox</button>
                              </div>

                              <div>
                                <button className="btn btn-link">Sent</button>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-5 mg-t-15 mg-sm-t-20 mg-lg-t-0">
                      <div className="card">
                        <div id="rs2" className="wd-100p ht-200"></div>
                        <div className="overlay-body pd-x-20 pd-t-20">
                          <div className="d-flex justify-content-between">
                            <div>
                              <h6 className="tx-12 tx-uppercase tx-inverse tx-bold mg-b-5">Inbox</h6>
                            </div>
                            <span className="col-md-8">
                              <InputGroup size="sm">
                                <Input placeholder="Search notes..." />
                                <InputGroupAddon><i className="icon ion-search"></i></InputGroupAddon>
                              </InputGroup>
                            </span>
                          </div>
                          <hr />
                            <ul>
                              <li>Message 1</li>
                              <li>Message 2</li>
                            </ul>
                          <hr/>
                          <p className="tx-12 mg-b-0">0-0/0</p>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-5 mg-t-15 mg-sm-t-20 mg-lg-t-0">
                      <div className="card">
                        <div id="rs3" className="wd-100p ht-200"></div>
                        <div className="overlay-body pd-x-20 pd-t-20">
                          <div className="d-flex justify-content-between">
                            <div>
                              <h6 className="tx-12 justify-content-between tx-bold mg-b-5">Select a message to view</h6>
                            </div>
                          </div>
                          <div style={{'textAlign':'center'}}>
                            <i className="icon ion-ios-email-outline tx-100 justify-center"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

              </div>

            </div>
        </div>
      </div>
    )
  }
}

class ComposeMailForm extends React.Component {
  render() {
    return (
      <div>
        <div className="card pd-5 pd-sm-5">

            <div className="form-group col-lg-12">
              <label className="form-control-label tx-12">To:</label>
              <Input className="form-control" bsSize="sm" type="text" name="firstname" />
            </div>

            <div className="form-group col-lg-12">
              <label className="form-control-label tx-12">Subject:</label>
              <Input className="form-control" bsSize="sm" type="text" name="firstname" />
            </div>

            <div className="form-group col-lg-12">
              <div className="">
                <textarea rows="7" className="form-control tx-12" placeholder="Message"></textarea>
              </div>
            </div>
            <div className="form-group col-lg-12">
              <Button className="btn btn-link"><i className="icon ion-android-attach"></i></Button>
              <Button className="btn btn-link"><i className="icon ion-android-person-add"></i></Button>
              <Button className="btn btn-link"><i className="icon ion-ios-timer-outline"></i></Button>
            </div>

        </div>
      </div>
    )
  }
}
