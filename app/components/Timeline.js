import React from 'react';
import { Row, Col } from 'react-bootstrap'
import { Sidebar, NavBar, SubContainer } from './'
import { Link } from 'react-router-dom'


export default class Timeline extends React.Component {
  render() {
    return (

      <div>
      <div className="am-mainpanel">
         <div className="am-pagetitle">
           <h5 className="am-title">Timeline</h5>

         </div>

         <div className="am-pagebody pd-0">
          <div className="row">
            <div className="col-md-9">
              <TimelineBody />
            </div>
            <div className="col-md-3 pd-0" style={{'border':'1px solid red'}}>
              <ContactList />
            </div>
          </div>
         </div>

       </div>
      </div>
    )
  }
}


<img src="https://www.atomix.com.au/media/2015/06/atomix_user31.png" className="wd-32 rounded-circle" alt="" />

function List() {
  return (
    <Row>
    </Row>
  );
}



class ContactList extends React.Component {
  render() {
    return (
      <div className="card pd-10">
        <List />
      </div>
      )
  }
}



class TimelineBody extends React.Component {
  render() {
    return (
    <div>
      Timeline Body
    </div>
    )
  }
}