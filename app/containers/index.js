export App from './App';
export HomePage from './HomePage';

export SignUpPage from './SignUpPage';
export ForgotPasswordPage from './ForgotPasswordPage';
export ResetPasswordPage from './ResetPasswordPage';

export DashboardPage from './DashboardPage';
export ProfilePage from './ProfilePage';
export EmployeesPage from './EmployeesPage'
export MessagesPage from './MessagesPage'
export NotesPage from './NotesPage'
export TasksPage from './TasksPage'
export LeavesPage from './LeavesPage'
export AnnouncementsPage from './AnnouncementsPage'
export TimelinePage from './TimelinePage'
