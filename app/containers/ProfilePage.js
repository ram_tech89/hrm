// @flow
import React, { Component } from 'react';
import { Profile } from '../components'

export default class ProfilePage extends Component {

  render() {
    return (
        <div>
          <Profile />
        </div>
    );
  }
}
