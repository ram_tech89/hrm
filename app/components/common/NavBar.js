import React from 'react'
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import { Link } from 'react-router-dom'

export default class NavBar extends React.Component {

    constructor(props) {
      super(props);

      this.toggle = this.toggle.bind(this);
      this.toggle1 = this.toggle1.bind(this);
      this.state = {
        dropdownOpen: false,
        dropdownOpen1: false
        };
      }

      toggle() {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
      }

      toggle1() {
        this.setState({
          dropdownOpen1: !this.state.dropdownOpen1
        });
      }
  render() {
  return (
    <div className="am-header">
    <div className="am-header-left">
      <span id="naviconLeft" href="" className="am-navicon d-none d-lg-flex"><i className="fa fa-bars"></i></span>
      <Link className="am-logo" to="/dashboard">Human Resource Management</Link>
    </div>

    <div className="am-header-right">
      <div className="dropdown dropdown-notification">

          <ButtonDropdown  isOpen={this.state.dropdownOpen} toggle={this.toggle}>
            <DropdownToggle style={{'backgroundColor':'inherit','border':'none'}}>
              <span className="nav-link pd-x-7 pos-relative">
                <i className="fa fa-bell-o tx-18"></i>
                <span className="square-8 bg-danger pos-absolute t-20 r--5 rounded-circle"></span>
              </span>
            </DropdownToggle>
            <DropdownMenu  className="dropdown-menu wd-300 pd-0-force">


            {/* This NotificationDropDown component is down below on the same page */}
                <NotificationDropDown />



            </DropdownMenu>
          </ButtonDropdown>

      </div>
      <div className="dropdown dropdown-profile">
        <span className="nav-link nav-link-profile">

          <ButtonDropdown  isOpen={this.state.dropdownOpen1} toggle={this.toggle1}>
            <DropdownToggle style={{'backgroundColor':'inherit','border':'none'}}>
              <img src="https://www.atomix.com.au/media/2015/06/atomix_user31.png" className="wd-32 rounded-circle" alt="" />
              <span className="logged-name"><span className="hidden-xs-down">John Doe</span> <i className="icon ion-ios-arrow-down"></i></span>
            </DropdownToggle>

            <DropdownMenu className="user-profile-nav wd-200 pd-x-10">

            {/* This UserLinks component is down below on the same page */}
              <UserLinks />


            </DropdownMenu>
          </ButtonDropdown>
        </span>

      </div>
    </div>
  </div>

  )
}
}


class NotificationDropDown extends React.Component{
  render() {
    return (
      <div>
      <div className="dropdown-menu-header">
        <label>Notifications</label>
        <Link to="/dashboard">Mark All as Read</Link>
      </div>

      <div className="media-list">

        <Link to="/dashboard" className="media-list-link read">
          <div className="media pd-x-20 pd-y-15">
            <img src="https://www.atomix.com.au/media/2015/06/atomix_user31.png" className="wd-40 rounded-circle" alt="" />
            <div className="media-body">
              <p className="tx-13 mg-b-0"><strong className="tx-medium">Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
              <span className="tx-12">October 03, 2017 8:45am</span>
            </div>
          </div>
        </Link>

        <Link to="/dashboard" className="media-list-link read">
          <div className="media pd-x-20 pd-y-15">
            <img src="https://www.atomix.com.au/media/2015/06/atomix_user31.png" className="wd-40 rounded-circle" alt="" />
            <div className="media-body">
              <p className="tx-13 mg-b-0"><strong className="tx-medium">Mellisa Brown</strong> appreciated your work <strong className="tx-medium">The Social Network</strong></p>
              <span className="tx-12">October 02, 2017 12:44am</span>
            </div>
          </div>
        </Link>
        <Link to="/dashboard" className="media-list-link read">
          <div className="media pd-x-20 pd-y-15">
            <img src="https://www.atomix.com.au/media/2015/06/atomix_user31.png" className="wd-40 rounded-circle" alt="" />
            <div className="media-body">
              <p className="tx-13 mg-b-0">20+ new items added are for sale in your <strong className="tx-medium">Sale Group</strong></p>
              <span className="tx-12">October 01, 2017 10:20pm</span>
            </div>
          </div>
        </Link>
        <Link to="/dashboard" className="media-list-link read">
          <div className="media pd-x-20 pd-y-15">
            <img src="https://www.atomix.com.au/media/2015/06/atomix_user31.png" className="wd-40 rounded-circle" alt="" />
            <div className="media-body">
              <p className="tx-13 mg-b-0"><strong className="tx-medium">Julius Erving</strong> wants to connect with you on your conversation with <strong className="tx-medium">Ronnie Mara</strong></p>
              <span className="tx-12">October 01, 2017 6:08pm</span>
            </div>
          </div>
        </Link>
        <div className="media-list-footer">
          <Link to="/dashboard" className="tx-12 media-list-link read"><i className="fa fa-angle-down mg-r-5"></i> Show All Notifications</Link>
        </div>
      </div>
    </div>

    )
  }
}

class UserLinks extends React.Component{
  render() {
    return (
      <ul className="list-unstyled user-profile-nav">
        <li><Link to="/dashboard/Profile"><i className="icon ion-ios-person-outline"></i> Profile</Link></li>
        <li><Link to="/dashboard/Profile"><i className="icon ion-ios-gear-outline"></i> Settings</Link></li>
        <li><Link to="/"><i className="icon ion-power"></i> Logout</Link></li>
      </ul>
    )
  }
}
