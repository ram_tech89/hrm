import React from 'react';
import { Sidebar, NavBar, SubContainer } from './'
import { Link } from 'react-router-dom'
import {
  Button,
  Input,
  ButtonGroup,
  InputGroup,
  InputGroupAddon,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  CardTitle,
  CardText,
  Row,
  Col,
  Table
 } from 'reactstrap'
 import classnames from 'classnames'


export default class Tasks extends React.Component {
  constructor(props) {
  super(props);

  this.toggle = this.toggle.bind(this);
  this.state = {
    activeTab: '1'
  };
}

toggle(tab) {
  if (this.state.activeTab !== tab) {
    this.setState({
      activeTab: tab
    });
  }
}
  render() {
    return (
      <div>
        <div className="mainbody">
          <div className="am-pagetitle">
            <h5 className="am-title">Tasks</h5>
          </div>
          {/*----------------main container of the page--------------*/}
            <div className="am-mainpanel">
              <div className="am-pagebody">
                <div className="card pd-sm-20">

                      <Nav tabs>
                        <NavItem>
                          <NavLink
                            className={classnames({ active: this.state.activeTab === '1' })}
                            onClick={() => { this.toggle('1'); }}
                          >
                            <span className="tx-bold">List</span>
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames({ active: this.state.activeTab === '2' })}
                            onClick={() => { this.toggle('2'); }}
                          >
                            <span  className="tx-bold">Kanban</span>
                          </NavLink>
                        </NavItem>
                      </Nav>
                      <TabContent activeTab={this.state.activeTab}>

                        <TabPane tabId="1">
                          <Tab1 />
                        </TabPane>

                        <TabPane tabId="2">
                          <Tab2 />
                        </TabPane>
                      </TabContent>

                </div>
              </div>
            </div>

        </div>
      </div>
    )
  }
}

class Tab1 extends React.Component {
  render() {
    return (
      <div>

        <div className="row">

            <div className="col-md-12">
            <br />
              <div className="row">
                <div className="col-md-2">
                  <div className="form-group mg-b-10-force">
                    <Input className="form-control wd-60 tx-12" type="select" bsSize="sm">
                      <option>10</option>
                      <option>20</option>
                      <option>50</option>
                      <option>100</option>
                    </Input>
                  </div>
                </div>

                <div className="col-md-1">

                </div>


                <div className="col-md-2">
                  <div className="form-group mg-b-10-force">
                    <Input className="form-control tx-12" type="select" bsSize="sm">
                      <option></option>
                    </Input>
                  </div>
                </div>

                <div className="col-md-2">
                  <div className="form-group mg-b-10-force">
                    <Input className="form-control tx-12" type="select" bsSize="sm">
                      <option>-Deadline-</option>
                    </Input>
                  </div>
                </div>

                <div className="col-md-2">
                  <div className="form-group mg-b-10-force">
                    <Input className="form-control tx-12" type="select" bsSize="sm">
                      <option>Status</option>
                    </Input>
                  </div>
                </div>

                <div className="col-md-3">
                  <InputGroup  size="sm" style={{'borderRadius':'0px'}}>
                    <Input placeholder="Search..." />
                    <InputGroupAddon><i className="icon ion-search"></i></InputGroupAddon>
                  </InputGroup>
                </div>

              </div>
            </div>

            <div className="col-md-8">

            </div>

            <div className="col-md-2">
              <div className="form-group mg-b-10-force">
                <Input className="form-control tx-12" type="select" bsSize="sm">
                  <option>-Project-</option>
                </Input>
              </div>
            </div>

            <div className="col-md-2">
              <div className="form-group mg-b-10-force">
                <Input className="form-control tx-12" type="select" bsSize="sm">
                  <option>-Milestone-</option>
                </Input>
              </div>
            </div>

            <div className="col-md-12">
              <Table inverse responsive>
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Start Date</th>
                    <th>Deadline</th>
                    <th>Project</th>
                    <th>Assigned to</th>
                    <th>Collaborators</th>
                    <th>Status</th>
                  </tr>
                </thead>
              </Table>
            </div>


            <div className="col-md-12 tx-12" style={{'textAlign':'center'}}>
                No record found.
            </div>
            <div className="col-md-12">
              < hr/>
                0-0/0
                <span className="pull-right">

                <ButtonGroup>
                  <Button className="btn-info" size="sm"><i className="icon ion-ios-rewind-outline"></i></Button>{' '}
                  <Button className="btn-info" size="sm"><i className="icon ion-ios-fastforward-outline"></i></Button>
                </ButtonGroup>

                </span>
            </div>
        </div>
      </div>
    )
  }
}

class Tab2 extends React.Component {
  render() {
    return (
      <div>

        <div className="row">
            <div className="col-md-12">
            <br />
              <div className="row">
                <div className="col-md-1">
                  <div className="form-group mg-b-10-force">
                    <Button className="btn btn-sm"><i className="icon ion-loop"></i></Button>
                  </div>
                </div>

                <div className="col-md-2">
                  <div className="form-group mg-b-10-force">
                    <Input className="form-control tx-12" type="select" bsSize="sm">
                      <option>-Project-</option>
                    </Input>
                  </div>
                </div>

                <div className="col-md-2">
                  <div className="form-group mg-b-10-force">
                    <Input className="form-control tx-12" type="select" bsSize="sm">
                      <option>-Milestone-</option>
                    </Input>
                  </div>
                </div>

                <div className="col-md-2">
                  <div className="form-group mg-b-10-force">
                    <Input className="form-control tx-12" type="select" bsSize="sm">
                      <option></option>
                    </Input>
                  </div>
                </div>

                <div className="col-md-2">
                  <div className="form-group mg-b-10-force">
                    <Input className="form-control tx-12" type="select" bsSize="sm">
                      <option>-Deadline-</option>
                    </Input>
                  </div>
                </div>

                <div className="col-md-3">
                  <InputGroup  size="sm" style={{'borderRadius':'0px'}}>
                    <Input placeholder="Search..." />
                    <InputGroupAddon><i className="icon ion-search"></i></InputGroupAddon>
                  </InputGroup>
                </div>

              </div>
            </div>
        </div>

        <div className="row">
        </div>
      </div>
    )
  }
}
