import React from 'react';
import { Row, Col } from 'react-bootstrap'
import { Sidebar, NavBar, SubContainer, AddEmployeeForm } from './'
import { Link } from 'react-router-dom'
import { Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'


export default class Employees extends React.Component {
  constructor(props) {
  super(props);
  this.state = {
    modal: false
  };

  this.toggle = this.toggle.bind(this);
}

toggle() {
  this.setState({
    modal: !this.state.modal
  });
}
  render() {
    return (
      <div>
        <div className="mainbody">

          <div className="am-pagetitle">
            <h5 className="am-title">Employees</h5>
          </div>

          <div className="am-mainpanel">
            <div className="am-pagebody">
              <div className="card pd-20 pd-sm-20">
                <div className="d-flex justify-content-between">

                  <div>
                    <h3 className="tx-20 tx-inverse tx-bold mg-b-5">Employees Data</h3>
                    <p className="tx-14">Description Here!</p>
                  </div>

                  <span>
                    <div>

                      {/*-----------------------------------------------Modal----------------------------------------------------*/}
                      <Button className="btn btn-info tx-14" onClick={this.toggle}><i className="icon ion-person-add"></i> Add New Employee</Button>

                      <Modal style={{'width' : '1000px','maxWidth':'1000px'}} isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                        <ModalHeader toggle={this.toggle}>Add New Employee</ModalHeader>
                        <ModalBody>
                          <AddEmployeeForm />
                        </ModalBody>
                        <ModalFooter>
                          <Button className="tx-12" color="info" onClick={this.toggle}><i className="fa fa-plus"></i>  Add</Button>{' '}
                          <Button className="tx-12" onClick={this.toggle}><i className="fa fa-close"></i> Cancel</Button>
                        </ModalFooter>
                      </Modal>
                      {/*--------------------------------------------Modal-Ends---------------------------------------------------*/}

                    </div>
                  </span>
                </div>

              {/* This TableStatic component is down below on the same page */}
              <TableStatic />
              </div>
            </div>
            
          </div>
        </div>
      </div>
    )
  }
}

class TableStatic extends React.Component {
  render() {
    return (
      <div className="table-wrapper">
        <Table hover responsive className="table-primary">
          <thead>
            <tr>
              <th className="wd-15p">Avatar</th>
              <th className="wd-15p">First name</th>
              <th className="wd-15p">Last name</th>
              <th className="wd-20p">Position</th>
              <th className="wd-15p">Start date</th>
              <th className="wd-10p">Salary</th>
              <th className="wd-25p">E-mail</th>
              <th className="wd-25p">Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><img src="https://pbs.twimg.com/profile_images/378800000079369168/3de6bcdff925153b31bea885111289f9_400x400.png" className="wd-32 rounded-circle" alt="" /></td>
              <td>Tiger</td>
              <td>Nixon</td>
              <td>System Architect</td>
              <td>2011/04/25</td>
              <td>$320,800</td>
              <td>t.nixon@datatables.net</td>
              <td><Link to="/dashboard/employees"><i className="fa fa-pencil"></i></Link>&nbsp;&nbsp;<Link to="/dashboard/employees"><i className="fa fa-remove" style={{'color':'red'}}></i></Link></td>
            </tr>
            <tr>
              <td><img src="https://cdn0.iconfinder.com/data/icons/PRACTIKA/256/user.png" className="wd-32 rounded-circle" alt="" /></td>
              <td>Garrett</td>
              <td>Winters</td>
              <td>Accountant</td>
              <td>2011/07/25</td>
              <td>$170,750</td>
              <td>g.winters@datatables.net</td>
              <td><Link to="/dashboard/employees"><i className="fa fa-pencil"></i></Link>&nbsp;&nbsp;<Link to="/dashboard/employees"><i className="fa fa-remove" style={{'color':'red'}}></i></Link></td>
            </tr>
            <tr>
              <td><img src="https://pbs.twimg.com/profile_images/378800000079369168/3de6bcdff925153b31bea885111289f9_400x400.png" className="wd-32 rounded-circle" alt="" /></td>
              <td>Ashton</td>
              <td>Cox</td>
              <td>Junior Technical Author</td>
              <td>2009/01/12</td>
              <td>$86,000</td>
              <td>a.cox@datatables.net</td>
              <td><Link to="/dashboard/employees"><i className="fa fa-pencil"></i></Link>&nbsp;&nbsp;<Link to="/dashboard/employees"><i className="fa fa-remove" style={{'color':'red'}}></i></Link></td>
            </tr>
            <tr>
              <td><img src="https://cdn0.iconfinder.com/data/icons/PRACTIKA/256/user.png" className="wd-32 rounded-circle" alt="" /></td>
              <td>Cedric</td>
              <td>Kelly</td>
              <td>Senior Javascript Developer</td>
              <td>2012/03/29</td>
              <td>$433,060</td>
              <td>c.kelly@datatables.net</td>
              <td><Link to="/dashboard/employees"><i className="fa fa-pencil"></i></Link>&nbsp;&nbsp;<Link to="/dashboard/employees"><i className="fa fa-remove" style={{'color':'red'}}></i></Link></td>
            </tr>
            <tr>
              <td><img src="https://pbs.twimg.com/profile_images/378800000079369168/3de6bcdff925153b31bea885111289f9_400x400.png" className="wd-32 rounded-circle" alt="" /></td>
              <td>Airi</td>
              <td>Satou</td>
              <td>Accountant</td>
              <td>2008/11/28</td>
              <td>$162,700</td>
              <td>a.satou@datatables.net</td>
              <td><Link to="/dashboard/employees"><i className="fa fa-pencil"></i></Link>&nbsp;&nbsp;<Link to="/dashboard/employees"><i className="fa fa-remove" style={{'color':'red'}}></i></Link></td>
            </tr>
          </tbody>
        </Table>
      </div>
    )
  }
}
